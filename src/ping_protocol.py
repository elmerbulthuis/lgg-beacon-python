"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""

from datetime import datetime
from ipaddress import ip_address
from typing import Optional

from nacl.exceptions import BadSignatureError  # type: ignore
from nacl.signing import SignedMessage, VerifyKey, SigningKey  # type: ignore
from nacl.encoding import Base64Encoder  # type: ignore
from twisted.internet.protocol import DatagramProtocol  # type: ignore

from database import Database
from models import (
    IP,
    Sample,
)
from wire_formats import (
    EchoClientServer,
    EchoClientServerType,
    EchoServerClient,
    EchoServerClientType,
)

MILLISECONDS = 1000  # Convert from seconds to milliseconds


class Echo(DatagramProtocol):
    def __init__(
        self,
        hsk: SigningKey,
        api_hsk_pub: Optional[VerifyKey],
        database: Database,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.hsk = hsk
        self.b64_verify_key = self.hsk.verify_key.encode(Base64Encoder)
        self.api_hsk_pub = api_hsk_pub
        self.database = database

    def datagramReceived(self, datagram, addr):
        # Validate size concerns
        timestamp = int(datetime.now().timestamp() * MILLISECONDS)
        if len(datagram) != 112:
            return
        inbound = EchoClientServer.from_buffer_copy(datagram)
        ip_addr = addr[0].encode()
        if not self.validate_inbound(inbound, timestamp, ip_addr):
            wire = self.null(timestamp)
        else:
            with self.database.write_txn() as txn:
                signature = self.hsk.sign(
                    inbound.ident + ip_addr, encoder=Base64Encoder
                )
                if inbound.type == EchoClientServerType.Initial:
                    wire, sample = self.handle_new(
                        ip_addr, inbound, timestamp, signature
                    )
                    self.database.put(txn, sample)
                elif (inbound.type == EchoClientServerType.Second) and (
                    sample := self.database.pop_incomplete(
                        txn, inbound.ident, inbound.seq
                    )
                ):
                    if sample.ip.addr != ip_addr:
                        return
                    wire, sample = self.handle_second(
                        inbound, sample, timestamp, signature
                    )
                    self.database.put(txn, sample)
                elif (inbound.type == EchoClientServerType.Final) and (
                    sample := self.database.pop_incomplete(
                        txn, inbound.ident, inbound.seq
                    )
                ):
                    if sample.ip.addr != ip_addr:
                        return
                    wire, sample = self.handle_final(
                        inbound, sample, timestamp, signature
                    )
                    self.database.finalise(txn, sample)
                else:
                    wire = self.null(timestamp)
        self.transport.write(
            bytes(wire),
            addr,
        )

    def validate_inbound(
        self, inbound: EchoClientServer, timestamp: float, ip: bytes
    ) -> bool:
        if (inbound.timestamp < timestamp - (43200 * MILLISECONDS)) or (
            inbound.timestamp > timestamp + (43200 * MILLISECONDS)
        ):
            return False
        if self.api_hsk_pub is not None:
            try:
                self.api_hsk_pub.verify(ip, Base64Encoder.decode(inbound.ident))
            except BadSignatureError:
                return False
        return True

    def handle_new(
        self,
        ip_addr: bytes,
        inbound: EchoClientServer,
        timestamp: float,
        signature: SignedMessage,
    ):
        sample = Sample(
            version=1,
            timestamp_c0=inbound.timestamp,
            timestamp_s0=timestamp,
            timestamp_c1=0,
            timestamp_s1=0,
            timestamp_c2=0,
            timestamp_s2=0,
            ip=IP(ip_addr, ip_address(ip_addr.decode()).version),
            ident=inbound.ident,
            seq=inbound.seq,
            pub=self.b64_verify_key,
        )
        packet = EchoServerClient(
            version=2,
            type=EchoServerClientType.Initial,
            seq=inbound.seq,
            signature=bytes(signature.signature),
            timestamp=timestamp,
        )
        return packet, sample

    def handle_second(
        self,
        inbound: EchoClientServer,
        sample: Sample,
        timestamp: float,
        signature: SignedMessage,
    ):
        sample.timestamp_c1 = inbound.timestamp
        sample.timestamp_s1 = timestamp
        packet = EchoServerClient(
            version=2,
            type=EchoServerClientType.Next,
            seq=sample.seq,
            signature=bytes(signature.signature),
            timestamp=timestamp,
        )
        return packet, sample

    def handle_final(
        self,
        inbound: EchoClientServer,
        sample: Sample,
        timestamp: float,
        signature: SignedMessage,
    ):
        sample.timestamp_c2 = inbound.timestamp
        sample.timestamp_s2 = timestamp
        packet = EchoServerClient(
            version=2,
            type=EchoServerClientType.Final,
            seq=sample.seq,
            signature=bytes(signature.signature),
            timestamp=timestamp,
        )
        return packet, sample

    def null(self, timestamp: float):
        return EchoServerClient(
            version=2,
            type=EchoServerClientType.Null,
            seq=0,
            signature=b"",
            timestamp=timestamp,
        )
